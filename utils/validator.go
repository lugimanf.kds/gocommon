package utils

import (
	"errors"
	"fmt"
	"github.com/go-playground/validator/v10"
	"strings"
)

func Validator(data interface{}) error {
	v:= validator.New()
	err := v.Struct(data)
	if err != nil{
		for _, x:= range err.(validator.ValidationErrors){
			tag := strings.ToLower(x.Tag())
			field:= strings.ToLower(x.Field())
			switch tag{
			case "required":
				return errors.New(fmt.Sprintf("please insert %v", field))
			default:
				return errors.New("there are field no valid, call administrator immediately")
			}
		}
	}
	return nil
}