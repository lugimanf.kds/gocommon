package middleware

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/lugimanf.kds/gocommon/redis"
	"gitlab.com/lugimanf.kds/gocommon/response"
)

func GetIPAndUserAgent(r *http.Request) (ip string, user_agent string) {
	ip = r.Header.Get("X-Forwarded-For")
	if ip == "" {
		ip = strings.Split(r.RemoteAddr, ":")[0]
	}

	user_agent = r.UserAgent()
	return ip, user_agent

}

func ThrottleRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ip, _ := GetIPAndUserAgent(r)
		keyUrl := []byte(fmt.Sprintf("%v_%v", r.Method, r.URL.Path))
		urlEncrypted := fmt.Sprintf("%x", md5.Sum(keyUrl))
		key := fmt.Sprintf("%v_%v", ip, urlEncrypted)
		maxRequestInLimitTime, err := strconv.Atoi(os.Getenv("MAX_REQUEST_IN_LIMIT_TIME"))
		if err != nil {
			response.Response(w, response.ParameterResponse{
				Status:  http.StatusInternalServerError,
				Message: "(ThrottleRequest) maxRequestInLimitTime Error",
				Data:    nil,
			})
			return
		}
		maxRequestLimitTimeInSecond, err := strconv.Atoi(os.Getenv("MAX_REQUEST_LIMIT_TIME_IS_SECOND"))
		if err != nil {
			response.Response(w, response.ParameterResponse{
				Status:  http.StatusInternalServerError,
				Message: "(ThrottleRequest) maxRequestLimitTimeInSecond Error",
				Data:    nil,
			})
			return
		}
		resultGetRedis, errGetCounter := redis.ConRedis.Get(key)
		if errGetCounter != nil {
			value := "1"
			resultGetRedis = &value
			redis.ConRedis.Set(key, "1", time.Duration(maxRequestLimitTimeInSecond)*time.Second)
		}
		counter, err := strconv.Atoi(*resultGetRedis)
		if err != nil {
			response.Response(w, response.ParameterResponse{
				Status:  http.StatusInternalServerError,
				Message: "(ThrottleRequest) Error",
				Data:    nil,
			})
			return
		}
		if maxRequestInLimitTime <= counter {
			response.Response(w, response.ParameterResponse{
				Status:  http.StatusTooManyRequests,
				Message: "Too many request",
				Data:    nil,
			})
			return
		}

		if errGetCounter == nil {
			err = redis.ConRedis.Incr(key)
			if err != nil {
				response.Response(w, response.ParameterResponse{
					Status:  http.StatusInternalServerError,
					Message: "(ThrottleRequest) Counter Key Error",
					Data:    nil,
				})
				return
			}
		}
		next.ServeHTTP(w, r)
	})
}
