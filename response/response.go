package response

import (
	"encoding/json"
	"net/http"
	"os"

	"github.com/unrolled/render"
	"gitlab.com/lugimanf.kds/gocommon/utils"
)

type JsonAPIResponse struct {
	StatusCode int         `json:"status_code"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
}

type ParameterResponse struct {
	Status  int
	Message string
	Data    interface{}
}

func Response(w http.ResponseWriter, data ParameterResponse) error {
	rdr := render.New()
	responseData := JsonAPIResponse{
		StatusCode: data.Status,
		Message:    data.Message,
		Data:       data.Data,
	}
	return rdr.JSON(w, data.Status, responseData)
}

func ResponseAes256(w http.ResponseWriter, data ParameterResponse) error {
	rdr := render.New()
	responseData := JsonAPIResponse{
		StatusCode: data.Status,
		Message:    data.Message,
		Data:       nil,
	}

	if data.Status >= 500 {
		responseData.Message = "Internal Error Please Call Administator"
		return rdr.JSON(w, data.Status, responseData)
	}

	keyString := os.Getenv("KEY_AES256")
	if keyString == "" {
		keyString = "default"
	}

	resultMarshal, err := json.Marshal(data.Data)
	if err != nil {
		responseData.StatusCode = 500
		responseData.Message = "Internal Error Please Call Administator"
		return rdr.JSON(w, http.StatusBadRequest, responseData)
	}
	resultAes256 := utils.EncryptAes256(string(resultMarshal), keyString)
	responseData.Data = resultAes256
	return rdr.JSON(w, data.Status, responseData)
}
