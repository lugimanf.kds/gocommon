package mysql

import (
	"database/sql"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type MysqlConInt interface {
	SetConnMaxLifetime(d time.Duration)
	SetMaxOpenConns(n int)
	SetMaxIdleConns(n int)
	Exec(query string, args ...interface{}) (sql.Result, error)
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
	Close() error
	Prepare(query string) (*sql.Stmt, error)
}

type MysqlConnection struct {
	Url      string
	Database string
}

var MySQLCon *MysqlConnection

func CreateConnection(MysqlUrl string) {
	MySQLCon = &MysqlConnection{
		Url: MysqlUrl,
	}
}

func (a *MysqlConnection) ConnectDB() (MysqlConInt, error) {
	//user:password@tcp(localhost:5555)/dbname
	Mysql, err := sql.Open("mysql", a.Url)

	if err != nil {
		return nil, err
	}
	Mysql.SetConnMaxLifetime(time.Minute * 3)
	Mysql.SetMaxOpenConns(10)
	Mysql.SetMaxIdleConns(10)

	return Mysql, nil

}
