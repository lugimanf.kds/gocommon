package redis

import (
	"time"

	"github.com/go-redis/redis"
)

type RedisInt interface {
	ConnectRedis() *redis.Client
	Get(Key string) (*string, error)
	Set(Key string, Val string, Duration time.Duration) error
	Expire(Key string, Duration time.Duration) error
	Incr(Key string) error
	Del(Key string) error
}

type RedisConnection struct {
	Url string
}

var ConRedis RedisInt

func CreateConnection(data RedisConnection) {
	ConRedis = &data
}

func (a *RedisConnection) ConnectRedis() *redis.Client {
	//"redis://<user>:<pass>@localhost:6379/<db>"
	opt, err := redis.ParseURL(a.Url)
	if err != nil {
		panic(err)
	}
	return redis.NewClient(opt)
}

func (a *RedisConnection) Get(Key string) (*string, error) {
	con := a.ConnectRedis()
	defer con.Close()
	val, err := con.Get(Key).Result()
	if err != nil {
		return nil, err
	}
	return &val, nil
}

func (a *RedisConnection) Set(Key string, Val string, Duration time.Duration) error {
	con := a.ConnectRedis()
	defer con.Close()
	err := con.Set(Key, Val, Duration).Err()
	if err != nil {
		return err
	}
	return nil
}

func (a *RedisConnection) Expire(Key string, Duration time.Duration) error {
	con := a.ConnectRedis()
	defer con.Close()
	err := con.Expire(Key, Duration).Err()
	if err != nil {
		return err
	}
	return nil
}

func (a *RedisConnection) Incr(Key string) error {
	con := a.ConnectRedis()
	defer con.Close()
	err := con.Incr(Key).Err()
	if err != nil {
		return err
	}
	return nil
}

func (a *RedisConnection) Del(Key string) error {
	con := a.ConnectRedis()
	defer con.Close()
	err := con.Del(Key).Err()
	if err != nil {
		return err
	}
	return nil
}
