package config

import (
	"os"
	"path/filepath"

	"github.com/joho/godotenv"
)

func SetupConfigEnv() {
	var BasePath, _ = os.Getwd()
	fileName := ".env"
	PathFileEnv := filepath.Join(BasePath, fileName)
	if _, err := os.Stat(PathFileEnv); !os.IsNotExist(err) {
		if err := godotenv.Load(PathFileEnv); err != nil {
			panic("Error loading " + fileName + " file")
		}
	} else {
		panic("File " + PathFileEnv + " not found")
	}
}
