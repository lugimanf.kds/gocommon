package curl

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

type CurlInt interface {
	Curl() ([]byte, *int, error)
	SetHeader(map[string]string)
	AddHeader(key, value string)
	SetUrl(url string)
	SetPayload(data interface{}) error
	SetTimeout(timeInSecond int)
}

type Curl struct{
	Url     string
	Method	string
	Header  map[string]string
	Payload io.Reader
	Timeout int
}

func NewCurl(url, method string) (CurlInt) {
	header := map[string]string{
		"Content-Type":  "application/json",
		"Accept":        "application/json",
	}

	return &Curl{
		Url: url,
		Header: header,
		Method: method,
		Timeout: 15,
	}
}

func(u *Curl) Curl() ([]byte, *int, error){
	var err error
	var client = &http.Client{
		Timeout: time.Duration(u.Timeout) * time.Second,
	}

	request, err := http.NewRequest(u.Method, u.Url, u.Payload)
	if err != nil {
		return nil, nil,  err
	}

	if u.Header != nil {
		for k, v := range u.Header {
			request.Header.Set(k, v)
		}
	}

	response, err := client.Do(request)
	if err != nil {
		return nil, nil, err
	}
	defer response.Body.Close()

	body, err:= ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, nil, err
	}

	return body, &response.StatusCode, nil
}

func(u *Curl) SetHeader(header map[string]string){
	u.Header = map[string]string{
		"Content-Type":  "application/json",
		"Accept":        "application/json",
	}
	for key, val:= range header{
		u.Header[key] = val
	}
}

func(u *Curl) AddHeader(key, value string){
	u.Header[key] = value
}

func(u *Curl) SetUrl(url string){
	u.Url = url
}

func(u *Curl) SetTimeout(timeInSecond int){
	u.Timeout = timeInSecond
}

func(u *Curl) SetPayload(data interface{}) error{
	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}
	u.Payload = bytes.NewBuffer(payload)
	return nil
}
